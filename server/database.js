const mongoose = require('mongoose');

const URI = 'mongodb://localhost/dbprueba';

mongoose.connect(URI, {useNewUrlParser:true, useUnifiedTopology: true}).then(db=>{
    console.log("Conectado DB");
}).catch(err => {
    console.log(err);
})


module.exports = mongoose;