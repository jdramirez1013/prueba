const express = require('express');
const app = express();
const morgan = require('morgan');


const { mongoose } = require('./database');

app.set('port', 3000);

app.use(morgan('dev'));

app.use(express.json());

app.use('/horasExtra', require('./service/horasExtra.service'))

app.listen(app.get('port'), () => {
    console.log("Server ", app.get('port'));
})