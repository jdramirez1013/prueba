const horasExtra = require('../model/horasExtra');

const horasExtraController = {};

horasExtraController.getHorasExtra = async (req, res) => {
    const horas = await horasExtra.find();
    res.json(horas);
}

horasExtraController.postHorasExtra = async (req, res) => {
    console.log(req.body);
    
    const horas = new horasExtra(req.body);
    await horas.save();

    res.json({status:'Horas Registradas'});
}

module.exports = horasExtraController;