const mongoose = require('mongoose');
const { Schema } = mongoose;

const horasExtra = new Schema({
    doc:{type:Number, required:true},
    nombre:{type:String, required:true},
    fechaInicio:{type:String, required:true},
    fechaFin:{type:String, required:true},
    inicioTurno:{type:String, required:true},
    finTurno: {type:String, required:true},
    inicioExtras: {type:String, required:true},
    finExtras: {type:String, required:true},
    motivo: {type:String, required:true}
})

module.exports = mongoose.model('horasExtra', horasExtra);