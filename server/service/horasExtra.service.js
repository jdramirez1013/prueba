const express = require('express');
const route = express.Router();
const horasExtraController = require('../controller/horasExtra.controller');

route.get('/', horasExtraController.getHorasExtra);
route.post('/', horasExtraController.postHorasExtra);

module.exports = route;
