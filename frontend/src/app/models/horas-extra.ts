export class HorasExtra {

    constructor(_id='', doc =0, nombre='',fechaInicio='', fechaFin='', inicioTurno = '', finTurno = '', inicioExtras = '', finExtras='', motivo=''){
        this._id = _id;
        this.doc = doc;
        this.nombre = nombre;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.inicioTurno = inicioTurno;
        this.finTurno = finTurno;
        this.inicioExtras = inicioExtras;
        this.finExtras = finExtras;
        this.motivo = motivo;
        
    }

    _id: string;
    doc:number;
    nombre:string;
    fechaInicio:string;
    fechaFin:string;
    inicioTurno:string;
    finTurno:string;
    inicioExtras: string;
    finExtras: string;
    motivo: string;
}
