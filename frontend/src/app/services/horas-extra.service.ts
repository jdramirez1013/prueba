import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { HorasExtra } from '../models/horas-extra';
import { from } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HorasExtraService {
  readonly URL = 'http://localhost:3000';

  horasExtra: HorasExtra;
  horas1: HorasExtra[];

  constructor(private http: HttpClient, ) {
    this.horasExtra = new HorasExtra();

   }
   private options = { headers: new HttpHeaders().set('Content-Type', 'application/json') };

   getHorasExtra(){
     return this.http.get(this.URL+"/horasExtra/");
   }

   createHorasExtra(horas : HorasExtra){
     console.log(horas);
     
     return this.http.post("http://localhost:3000/horasExtra", horas, this.options);
   }
}
