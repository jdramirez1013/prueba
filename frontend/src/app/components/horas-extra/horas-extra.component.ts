import { Component, OnInit } from '@angular/core';
import { NgForm, ReactiveFormsModule } from '@angular/forms';
import { HorasExtraService } from '../../services/horas-extra.service'
import { from } from 'rxjs';
import {HorasExtra} from '../../models/horas-extra'


@Component({
  selector: 'app-horas-extra',
  templateUrl: './horas-extra.component.html',
  styleUrls: ['./horas-extra.component.css'], 
  providers: [HorasExtraService]
})
export class HorasExtraComponent implements OnInit {

  horasExtra: HorasExtra;
  constructor(private horasExtraService:HorasExtraService) { }

  ngOnInit(): void {
    this.horasExtra = new HorasExtra();
  }


  addHoras(form?:NgForm){
    console.log(form.value);
    console.log(this.horasExtra);

    this.horasExtraService.createHorasExtra(form.value).subscribe(res=>{
        console.log(res);
    })
    
  }

}
